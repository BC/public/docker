#!/bin/bash
set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes

# Function to display script usage
usage() {
    echo "Usage: $0 <snake_case_value> [OPTIONS]"
    echo "We expect the dockerfile name in <snake_case_value>, without full path nor extension. Ex: debian12-php"
    echo "Options:"
    echo "  --reason, -m <reason>  Specify a reason for the docker image build/push (optional, interactive if not set)"
    echo "  --build, -b            Enable docker build (boolean, optional, interactive if not set)"
    echo "  --push, -p             Enable pushing the docker image on the registry, with current datetime and [reason] in tag (boolean, optional, interactive if not set)"
    echo "  --run, -r              Enable docker run (boolean, optional, interactive if not set, can't be set if --build or --push is set)"
    echo "  --help, -h             Display this help message and exit"
    exit 1
}

# Initialize variables to store argument values
dockerfile=""
reason=""
build=false
push=false
run=false

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        -h|--help)
            usage
            ;;
        -*)
            case "$1" in
                --reason|-m)
                    shift
                    if [[ $# -eq 0 || "${1:0:1}" == "-" ]]; then
                        if [[ $# -eq 0 ]]; then
                            usage
                            exit 1
                        fi
                    else
                        reason="$1"
                    fi
                    ;;
                --build|-b)
                    build=true
                    ;;
                --push|-p)
                    push=true
                    ;;
                --run|-r)
                    if $build || $push; then
                        echo "Error: --run cannot be set if --build or --push is set."
                        usage
                        exit 1
                    fi
                    run=true
                    ;;
                *)
                    echo "Invalid option: $1"
                    usage
                    ;;
            esac
            shift
            ;;
        *)
            if [[ -z "$dockerfile" ]]; then
                dockerfile="$1"
            else
                echo "Invalid argument: $1"
                usage
            fi
            shift
            ;;
    esac
done

if [[ -z "$dockerfile" ]]; then
    usage
    exit 1
elif [[ $build == false && $push == false && $run == false ]]; then
    read -p "Do you want to build the image ? (y/n)" input
    will_be_image_build="$input"
    if [[ $will_be_image_build == "y" ]]; then
        build=true
    else
      build=false
    fi
    read -p "Do you want to run the image ? (y/n)" input
    will_be_image_run="$input"
    if [[ $will_be_image_run == "y" ]]; then
        run=true
    else
        run=false
    fi
    if [[ $run == false && build == true ]]; then
        read -p "Do you want to push the image after build ? (y/n)" input
        will_be_image_pushed="$input"
        if [[ $will_be_image_pushed == "y" ]]; then
            push=true
        else
            push=false
        fi
    else
        echo "Image will be ran, and not pushed to the docker registry"
    fi
fi




# Example usage of the arguments
echo "Dockerfile: $dockerfile"
echo "Reason: $reason"
echo "Build: $build"
echo "Push: $push"
echo "Run: $run"

if $build; then
    echo "📦🏗️ Building the image, without cache and with pull"
    docker build --rm --no-cache --pull -f dockers/$dockerfile.docker -t "gitlab.linphone.org:4567/bc/public/docker/${dockerfile}:dev" .
    if [[ $? -ne 0 ]]; then
      echo "📦🏗️ Image build ended in error"
    else
      echo "📦✅ Image built"
    fi
fi

if $push; then
    if [[ -n "$reason" ]]; then
        echo "Used $reason as part of the image tag"
    else
      read -p "Enter reason for the image build/push shortly, in snake_case (ex: debian12_packaging): " input
      reason="$input"
    fi

    currentDateTime=$(date +%Y%m%d_%H%M%S)

    docker image tag gitlab.linphone.org:4567/bc/public/docker/${dockerfile}:dev gitlab.linphone.org:4567/bc/public/docker/${dockerfile}:${currentDateTime}_${reason}

    echo "📦⬆️ Shipping it..."
    docker push gitlab.linphone.org:4567/bc/public/docker/${dockerfile}:${currentDateTime}_${reason}
fi

if $run; then
    echo "📦#️⃣ Entering in the image"
    docker run -it --volume=${PWD}:/home/bc gitlab.linphone.org:4567/bc/public/docker/${dockerfile}:dev bash -l
fi
